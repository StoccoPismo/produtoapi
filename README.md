Como fazer o deploy da compraApi.


 Os recursos da Api de produtos estao liberados para todos
         caso queira ver a lista de apis: http://localhost:8080/swagger-ui.html
         
         
Para ser mais tranquilo, estou utilizando um DB em memória. Caso queira mudar basta apenas ir no arquivo
    application.properties e mudar as configuracões para estas abaixo:
    
    #DB Banco de teste
    spring.datasource.url=jdbc:mysql://URL DO MYSQL DB
    spring.datasource.username=USERNAME
    spring.datasource.password=PASSWORD
    spring.datasource.driver-class-name=com.mysql.jdbc.Driver
    spring.datasource.max-idle=1800
    spring.datasource.maximum-pool-size=50
    spring.datasource.max-active=60
    spring.datasource.testOnBorrow=true
    spring.datasource.validationQuery=SELECT 1
    spring.mvc.dispatch-options-request=true
    
    #Hibernate Configuration:
    spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5Dialect
    spring.jpa.show-sql=true
    spring.jpa.hibernate.naming-strategy=org.hibernate.cfg.EJB3NamingStrategy
    spring.jpa.properties.hibernate.format_sql=false
    spring.jpa.properties.hibernate.enable_lazy_load_no_trans=true
    spring.jpa.hibernate.ddl-auto=update
    

Instale o graddle em sua máquina.
    https://gradle.org/install
    
No root do projeto use: **gradle wrapper**


Para buildar o projeto use
    gradle build
    
    
Para buildar sem verificacao de testes use


    gradle build -x test


Para buildar sem verificacao de testes use: **gradle build -x test**

agora é só usar **java -jar build/libs/nomeDoArquivo.jar**

