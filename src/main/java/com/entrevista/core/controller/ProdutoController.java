package com.entrevista.core.controller;

import com.entrevista.core.dto.ProdutoDTO;
import com.entrevista.core.dto.ProdutoDoCarrinhoDTO;
import com.entrevista.core.service.ProdutoService;
import com.entrevista.core.utils.TextUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by stocco on 24/06/17.
 */
@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private ProdutoService produtoService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> cadastrarProduto(@Validated @RequestBody ProdutoDTO produtoDTO){

        try {
            produtoService.saveProduto(produtoDTO);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage("Produto cadastrado!"), HttpStatus.OK);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> atualizaProduto(@Validated @RequestBody ProdutoDTO produtoDTO){
        try{

            produtoService.updateProduto(produtoDTO);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage("Produto atualizado!"), HttpStatus.OK);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(value = "/atualizaEstoquePosCompra", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> atualizaEstoquePosCompra(@Validated @RequestBody List<ProdutoDoCarrinhoDTO> produtoDTO){
        try{
            produtoService.atualizaEstoquePosCompra(produtoDTO);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage("Transacao Realizada!"), HttpStatus.OK);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @RequestMapping(value="/nome/{nome}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getProdutoByNome(@PathVariable("nome") String nome){

        try{
            ProdutoDTO produtoDTO = produtoService.getProdutoByName(nome);
            if(produtoDTO == null)
                return new ResponseEntity(TextUtils.apiAnswerReturnMessage("Produto nao encontrado!"), HttpStatus.NO_CONTENT);

            return new ResponseEntity(TextUtils.objectToJsonFormat(produtoDTO), HttpStatus.OK);

        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getTodosProdutos(){
        try{
            return new ResponseEntity(TextUtils.objectToJsonFormat(produtoService.getAllProdutos()), HttpStatus.OK);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getProdutoById(@PathVariable("id") Long id){

        try{
            ProdutoDTO produtoDTO = produtoService.getProdutoById(id);
            if(produtoDTO != null)
                return new ResponseEntity(TextUtils.objectToJsonFormat(produtoDTO), HttpStatus.OK);

            return new ResponseEntity(TextUtils.apiAnswerReturnMessage("Produto nao encontrado!"), HttpStatus.OK);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostConstruct
    private void init() {
        try {

            if (produtoService.getProdutoByName("produtoInicial") == null) {

                ProdutoDTO produtoDTO = new ProdutoDTO()
                        .setNome("produtoInicial")
                        .setPreco(10.25)
                        .setQuantidade(20L)
                        .setAtivo(true);
                produtoService.saveProduto(produtoDTO);

                produtoDTO = new ProdutoDTO()
                        .setNome("produtoInicial2")
                        .setPreco(2.50)
                        .setQuantidade(200L)
                        .setAtivo(true);

                produtoService.saveProduto(produtoDTO);

            }

        }
        catch (Exception e){
            logger.error(e.getMessage(), e);
        }
    }






}
