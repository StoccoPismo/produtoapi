package com.entrevista.core.dto;


import javax.validation.constraints.NotNull;

/**
 * Created by stocco on 24/06/17.
 */
public class ProdutoDoCarrinhoDTO {
    @NotNull
    private Long produtoDoCarrinhoID;
    @NotNull
    private Long quantidadeDeProdutosNoCarrinho;



    public Long getProdutoDoCarrinhoID() {
        return produtoDoCarrinhoID;
    }

    public ProdutoDoCarrinhoDTO setProdutoDoCarrinhoID(Long produtoDoCarrinhoID) {
        this.produtoDoCarrinhoID = produtoDoCarrinhoID;
        return this;
    }

    public Long getQuantidadeDeProdutosNoCarrinho() {
        return quantidadeDeProdutosNoCarrinho;
    }

    public ProdutoDoCarrinhoDTO setQuantidadeDeProdutosNoCarrinho(Long quantidadeDeProdutosNoCarrinho) {
        this.quantidadeDeProdutosNoCarrinho = quantidadeDeProdutosNoCarrinho;
        return this;
    }
}
