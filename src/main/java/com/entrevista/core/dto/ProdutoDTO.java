package com.entrevista.core.dto;

import com.entrevista.core.entity.Produto;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by stocco on 24/06/17.
 */
public class ProdutoDTO {

    private Long id;

    @NotNull
    private Long quantidade;

    @NotEmpty
    private String nome;

    @NotNull
    private Double preco;

    private boolean ativo;



    public boolean isAtivo() {
        return ativo;
    }

    public ProdutoDTO setAtivo(boolean ativo) {
        this.ativo = ativo;
        return this;

    }

    public Long getId() {
        return id;
    }

    public ProdutoDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getQuantidade() {
        return quantidade;
    }

    public ProdutoDTO setQuantidade(Long quantidade) {
        this.quantidade = quantidade;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public ProdutoDTO setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public Double getPreco() {
        return preco;
    }

    public ProdutoDTO setPreco(Double preco) {
        this.preco = preco;
        return this;
    }
}
