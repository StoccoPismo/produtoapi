package com.entrevista.core.service;

import com.entrevista.core.dto.ProdutoDTO;
import com.entrevista.core.dto.ProdutoDoCarrinhoDTO;
import com.entrevista.core.entity.Produto;
import com.entrevista.core.repository.ProdutoRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stocco on 24/06/17.
 */
@Service
public class ProdutoService {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private ProdutoRepository produtoRepository;

    @Transactional()
    public void saveProduto(ProdutoDTO produtoDTO) throws Exception{

        Produto produto = produtoRepository.findByNome(produtoDTO.getNome());
        if(produto != null)
            throw new Exception("Produto com mesmo nome já cadastrado");

        produto = new Produto();
        BeanUtils.copyProperties(produtoDTO, produto, "id");
        produtoRepository.save(produto);

    }

    @Transactional(readOnly = true)
    public List<ProdutoDTO> getAllProdutos() throws Exception{

        List<ProdutoDTO> produtoDTOS = new ArrayList<>();
        for (Produto produto : produtoRepository.findAll()) {
            ProdutoDTO produtoDTO = new ProdutoDTO();
            BeanUtils.copyProperties(produto, produtoDTO);
            produtoDTOS.add(produtoDTO);
        }

        return  produtoDTOS;
    }

    @Transactional(readOnly = true)
    public ProdutoDTO getProdutoByName(String nome) throws Exception{

        Produto produto = produtoRepository.findByNome(nome);
        if(produto == null)
            return null;

        ProdutoDTO produtoDTO = new ProdutoDTO();
        BeanUtils.copyProperties(produto, produtoDTO);
        return  produtoDTO;
    }

    @Transactional(readOnly = true)
    public ProdutoDTO getProdutoById(Long id) throws Exception{

        Produto produto = produtoRepository.findOne(id);
        if(produto == null)
            return null;

        ProdutoDTO produtoDTO = new ProdutoDTO();
        BeanUtils.copyProperties(produto, produtoDTO);
        return  produtoDTO;
    }


    @Transactional()
    public void updateProduto(ProdutoDTO produtoDTO) throws Exception{

        if(produtoDTO.getId() == null)
            throw new Exception("Produto sem Id");

        Produto produto = produtoRepository.findOne(produtoDTO.getId());
        BeanUtils.copyProperties(produtoDTO, produto);
        produtoRepository.save(produto);

    }

    @Transactional()
    public void atualizaEstoquePosCompra(List<ProdutoDoCarrinhoDTO> produtoDTOS){

        produtoDTOS.forEach(produtoDTO ->
            produtoRepository.atualizaQuantidadeDeProdutoAposCompra(produtoDTO.getProdutoDoCarrinhoID(), produtoDTO.getQuantidadeDeProdutosNoCarrinho())
        );

    }


}
