package com.entrevista.core.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by stocco on 24/06/17.
 */
public class TextUtils {

    private static final Logger logger = LoggerFactory.getLogger(TextUtils.class);

    public static String apiAnswerReturnMessage(String message){
        return "{\"message\":\""+message+"\"}";
    }

    public static String objectToJsonFormat(Object DTO){

        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        try {
            return mapper.writeValueAsString(DTO);
        }
        catch (Exception exception){
            logger.error(exception.getMessage(), exception);
            return apiAnswerReturnMessage("Json_Parser_Error");
        }

    }
}
