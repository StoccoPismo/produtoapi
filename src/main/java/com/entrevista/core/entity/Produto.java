package com.entrevista.core.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Produto {

    @GeneratedValue( strategy = GenerationType.AUTO)
    @Id
    private Long id;
    private Long quantidade;

    private String nome;

    private Double preco;

    private boolean ativo;


    public boolean isAtivo() {
        return ativo;
    }

    public Produto setAtivo(boolean ativo) {
        this.ativo = ativo;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public Produto setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Produto setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getQuantidade() {
        return quantidade;
    }

    public Produto setQuantidade(Long quantidade) {
        this.quantidade = quantidade;
        return this;
    }

    public Double getPreco() {
        return preco;
    }

    public Produto setPreco(Double preco) {
        this.preco = preco;
        return this;
    }
}
