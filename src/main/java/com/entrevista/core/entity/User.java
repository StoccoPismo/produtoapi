package com.entrevista.core.entity;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class User implements Serializable, AbstractUser {

    private static final long serialVersionUID = -2878615835482202018L;

    @GeneratedValue( strategy = GenerationType.AUTO)
    @Id
    private Long id;

    private String name;
    private String email;
    private String password;


    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public Long getId() {
        return id;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }


    public User encriptPassword() {
        this.password = encriptPassword(this.password);
        return this;
    }

    public String encriptPassword(String value) {
        ShaPasswordEncoder encoder = new ShaPasswordEncoder(256);
        value = encoder.encodePassword(value, null);
        return value;
    }

}