package com.entrevista.core.repository;

import com.entrevista.core.entity.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by stocco on 24/06/17.
 */
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

    Produto findByNome(String nome);

    @Modifying
    @Query(value = "update produto as p set p.quantidade = p.quantidade - :quantidadeComprada where p.id = :id", nativeQuery = true)
    Integer atualizaQuantidadeDeProdutoAposCompra(@Param("id") Long id, @Param("quantidadeComprada") Long quantidadeComprada);

}
