package com.entrevista.core.repository;

import com.entrevista.core.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by stocco on 12/03/17.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);
}
