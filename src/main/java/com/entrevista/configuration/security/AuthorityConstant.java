package com.entrevista.configuration.security;

public class AuthorityConstant {

	public static final String ROLE_USUARIO = "ROLE_USUARIO";
	public static final String ROLE_ADMINISTRADOR = "ROLE_ADMINISTRADOR";
	
}
