package com.entrevista.configuration.security;


import com.entrevista.core.entity.User;
import com.entrevista.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Transactional
@Service("myUserDetailsService")
public class MyUserDetailsService implements UserDetailsService {


    @Autowired
    private UserRepository userRepository;

    @Override
    public org.springframework.security.core.userdetails.UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {

        User user = userRepository.findByEmail(email);

        List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList(AuthorityConstant.ROLE_USUARIO);
        UserDetails userDetails = new UserDetails(user, true, true, true, true, authorityList );
        userDetails.setEmail(user.getEmail());
        userDetails.setId(user.getId());
        userDetails.setName(user.getName());
        return userDetails;
    }



}