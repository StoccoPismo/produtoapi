package com.entrevista.configuration.security;


import com.entrevista.core.entity.User;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by stocco on 26/03/17.
 */
public class UserContext {

    public static User getLoggedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = null;

        if (authentication != null) {
            principal = authentication.getPrincipal();
            User currentCyclist = new User();
            BeanUtils.copyProperties(principal, currentCyclist);
            return currentCyclist;
        }

        return null;

    }

}
