package com.entrevista.configuration.init;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;

public class LocalResourceBundleMessage extends
        ReloadableResourceBundleMessageSource {
	
	public String getMessage(String code) {
		return this.getMessage(code, null,  null);
	}

}
