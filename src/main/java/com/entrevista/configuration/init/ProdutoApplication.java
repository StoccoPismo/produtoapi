package com.entrevista.configuration.init;

import com.entrevista.configuration.security.HeaderHiddenHttpMethodFilter;
import com.entrevista.configuration.security.SecurityConfig;
import com.google.common.base.Predicates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.ErrorPageFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Date;
import java.util.Locale;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.entrevista.core.repository"})
@EntityScan(basePackages = {"com.entrevista.core.*", "com.entrevista.configuration.converter"})
@EnableAutoConfiguration
@Import(SecurityConfig.class)
@Configuration
@EnableSwagger2
public class ProdutoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProdutoApplication.class, args);
	}


	@Autowired
	private Environment env;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public LocaleResolver localeResolver() {
		final CookieLocaleResolver ret = new CookieLocaleResolver();
		ret.setDefaultLocale(new Locale("pt_BR"));
		return ret;
	}

	@Bean
	public javax.servlet.Filter hiddenHttpMethodFilter() {
		HeaderHiddenHttpMethodFilter filter = new HeaderHiddenHttpMethodFilter();
		return filter;
	}


    @Bean
    public Docket mainConfig() {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
                .paths(Predicates.not(PathSelectors.regex("/error.*"))).build().pathMapping("/")
                .directModelSubstitute(Date.class, String.class)
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .genericModelSubstitutes(ResponseEntity.class);
    }


	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("API Produto")
				.description("Essa é a API do sistema de pRODUTOS")
				.build();
	}




}
