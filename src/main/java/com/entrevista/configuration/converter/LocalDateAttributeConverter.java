package com.entrevista.configuration.converter;

import org.joda.time.LocalDate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;

@Converter(autoApply = true)
public class LocalDateAttributeConverter implements AttributeConverter<LocalDate, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDate value) {
        if(value==null) return null;
        return new Date(value.toDate().getTime());
    }

    @Override
    public LocalDate convertToEntityAttribute(Date value) {
        if(value==null) return null;
        return new LocalDate(value);
    }
}
