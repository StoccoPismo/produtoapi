package com.entrevista.core.service;

import com.entrevista.ProdutoApplicationTests;
import com.entrevista.core.dto.ProdutoDTO;
import com.entrevista.core.dto.ProdutoDoCarrinhoDTO;
import com.entrevista.core.entity.Produto;
import com.entrevista.core.repository.ProdutoRepository;
import com.entrevista.core.utils.RandomizingUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

/**
 * Created by stocco on 24/06/17.
 */
public class ProdutoServiceTest extends ProdutoApplicationTests{

    @Autowired
    private ProdutoRepository produtoRepository;

    private ProdutoDTO produtoDTO;

    private Long idTestAtualizaEstoquePosCompra;
    private Long idAtualizaEstoquePosCompra2;
    private Long idTestUpdateProduto;
    private Long idTestFindByNome;

    @Before
    public void setUp() throws Exception {

        this.produtoDTO = RandomizingUtil.getRandomizer().nextObject(ProdutoDTO.class).setId(null);


        Produto produto = new Produto()
                .setNome("test")
                .setPreco(10.0)
                .setQuantidade(2L)
                .setAtivo(true);
        idTestAtualizaEstoquePosCompra = produtoRepository.save(produto).getId();

        Produto produto2 = new Produto()
                .setNome("test2")
                .setPreco(10.0)
                .setQuantidade(10L)
                .setAtivo(true);
        idAtualizaEstoquePosCompra2 = produtoRepository.save(produto2).getId();

        Produto produto3 = new Produto()
                .setNome("test3")
                .setPreco(10.0)
                .setQuantidade(10L)
                .setAtivo(true);
        idTestUpdateProduto = produtoRepository.save(produto3).getId();


    }

    @Test
    public void getAllProdutos() throws Exception {
        Assert.assertTrue(produtoRepository.findAll().size() > 2);
    }

    @Test
    public void getProdutoByName() throws Exception {

        Produto produto4 = new Produto()
                .setNome("meuNomeDeTeste")
                .setPreco(10.0)
                .setQuantidade(10L)
                .setAtivo(true);
        idTestFindByNome = produtoRepository.save(produto4).getId();

        Assert.assertEquals(idTestFindByNome, produtoRepository.findByNome("meuNomeDeTeste").getId());
    }

    @Test
    public void getProdutoById() throws Exception {
        Assert.assertEquals("test", produtoRepository.findOne(idTestAtualizaEstoquePosCompra).getNome());
    }

    @Test
    public void updateProduto() throws Exception {

        Produto produto = produtoRepository.findOne(idTestUpdateProduto);
        Long valorAntigo = produto.getQuantidade();
        produto.setQuantidade(1000L);
        Produto produtoAtualizado = produtoRepository.save(produto);
        Assert.assertNotSame(valorAntigo.longValue(), produtoAtualizado.getQuantidade().longValue());

    }

    @Test
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void atualizaEstoquePosCompra() throws Exception {

        ProdutoDoCarrinhoDTO produto1 = new ProdutoDoCarrinhoDTO()
                .setProdutoDoCarrinhoID(idTestAtualizaEstoquePosCompra)
                .setQuantidadeDeProdutosNoCarrinho(1L);

        ProdutoDoCarrinhoDTO produto2 = new ProdutoDoCarrinhoDTO()
                .setProdutoDoCarrinhoID(idAtualizaEstoquePosCompra2)
                .setQuantidadeDeProdutosNoCarrinho(10L);

        for (ProdutoDoCarrinhoDTO produtoParaSerAtualizado: Arrays.asList(produto1, produto2)) {
           Integer atualizou = produtoRepository.atualizaQuantidadeDeProdutoAposCompra(produtoParaSerAtualizado.getProdutoDoCarrinhoID(), produtoParaSerAtualizado.getQuantidadeDeProdutosNoCarrinho());
           Assert.assertEquals(1, atualizou.intValue());
        }

    }

    @Test
    public void saveProduto() throws Exception {

        Produto produto = produtoRepository.findByNome(produtoDTO.getNome());
        Assert.assertNull(produto);

        produto = new Produto();
        BeanUtils.copyProperties(produtoDTO, produto, "id");
        produtoRepository.save(produto);

        Produto produtoSalvo = produtoRepository.findByNome(produtoDTO.getNome());
        Assert.assertNotNull(produtoSalvo);

    }



}